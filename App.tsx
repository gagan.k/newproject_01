import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import ActionCard from './components/ActionCard'
import ContactList from './components/ContactList'
import ElevatedCards from './components/ElevatedCards'
import FancyCard from './components/FancyCard'
import FlatCards from './components/FlatCard'

const App = () => {
  return (
    <ScrollView>
       <FlatCards/>
       <ElevatedCards/>
       <FancyCard/>
       <ActionCard/>
       <ContactList/>
    </ScrollView>
  )
}

export default App

const styles = StyleSheet.create({})